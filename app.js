
const VERSION='8';
var CACHE_NAME='album-v1';
var urlsToCache = [
  '/',
  '/index.html',
  '/utils.js',
  '/img/favicon.png',
  '/img/icon128.png',
  '/img/home.png',
  '/img/plus-square.png',
  '/img/plus-circle.png',
  '/img/download.png',
  '/img/home.png',
  '/img/share-2.png',
  '/img/eye-off.png',
  '/img/trash-2.png',
  '/css/normalize.css',
  '/css/skeleton.css',
  '/css/style.css'
];

self.addEventListener('install', function(event){
  event.waitUntil(
    caches.open(CACHE_NAME)
    .then(function(cache){
      console.log('Opened cache');
      return cache.addAll(urlsToCache);
    })
    .catch(function(err){
    console.log('Error in cache', err);
  })
  );
});


const ALBUMS_LIST = '/albumsList';

// see: https://developer.mozilla.org/en-US/docs/Web/API/Clients/claim
self.addEventListener('activate', event => {
  event.waitUntil(clients.claim());
});

self.addEventListener('fetch', function(event) {

  const {
    request,
    request: {
      url,
      method,
    },
  } = event;
  
  console.log(url+" trying internal fetch ("+ method+")" );

  if (url.match('/addToCache#')) {
      var photoSrc = url.split('#').pop();
      caches.open(CACHE_NAME)
      .then(function(cache){
          console.log(url + ' Opened cache to add: '+photoSrc);
          cache.add(photoSrc).then(function(){
              console.log(url + ' Stored: '+photoSrc);
              return new Response('{}');
          });
      })
      .catch(function(err){
          console.log('Error in cache for image', err);
          return new Response('{}');
      })
      event.respondWith( new Response('{}') );
  } else if (url.match(ALBUMS_LIST)) {
      if (method === 'POST') {
          request.json().then(body => {
              caches.open(ALBUMS_LIST).then(function(cache) {
                  cache.put(ALBUMS_LIST, new Response(JSON.stringify(body)));
        	});
              });
          console.log(url + "album list saved");
          event.respondWith( new Response('{}') );
      } else {
	event.respondWith( caches.open(ALBUMS_LIST).then(function(cache) {
	  return cache.match(ALBUMS_LIST).then(function (response) {
	    console.log(url + 'Opened ', ALBUMS_LIST, ' from cache');
	    return response || new Response('{}');
	  }) || new Response('{}');
	}));
        console.log(url + "album list read");
      }
  } else if (url.match('/photosList/')) {
      var album = url.split('/').pop();
      //network first, cache if not there
      event.respondWith( 
          fetch("photoList.php?album="+album).then(function(data) {
              if(!data || data.status !== 200 || data.type !== 'basic') {
                  if(data) console.log(url + " found from network -> " + data.status);
                  else console.log(url + " found from network -> no reply");
                  return caches.match(event.request)
                               .then(function(response) {
                                   console.log(url + " found in cache? -> "+response);
                                   return response;
                               }) || data;
              } else {
                  console.log(url + " found from network -> " + data.status);
                  var responseToCache = data.clone();
                  caches.open(CACHE_NAME)
                        .then(function(cache) {
                            cache.put(event.request, responseToCache);
                        });
                  return data;
              }
          })
          .catch((error) => {
              console.log(url + ' Network error in fetch: ', error);
              return caches.match(event.request)
                           .then(function(response) {
                               console.log(url + " found in cache? -> "+response);
                               return response;
                           }) || new Response('{}');;
          })
      );
  } else if (url.match("/version")) {
	    var j = JSON.stringify({v:VERSION});
	    var r = new Response(j);
	    console.log(url + "version read: "+j);
	    event.respondWith( r );
  } else {
      console.log(url+" no fetch overide, standard fetch from cache/network will be needed.");
      event.respondWith(
      caches.match(event.request).then(function(response) {
        if(response) console.log(url + " (cache) -> " + response.status);
        else console.log(url + " (cache) -> null");
        return response || fetch(event.request).then(
                             function(response) {
                               console.log(url + " (network) -> " + response.status);
                               return response;
                             })
                             .catch((error) => {
                                 console.log(url + ' Network error in fetch: ', error);
                                 return new Response('Network access is mandatory for this action');
          });
      })
    );
  }


});

console.log('Service worker vX.Y.'+VERSION+' fully registered');

