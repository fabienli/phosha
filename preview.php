<?php
// Fichier et nouvelle taille
$filename = $_GET['photo'];
$previewalert = isset($_GET['a']);
$percent = 0.5;

// Content type
header('Content-Type: image/jpeg');

// Calcul des nouvelles dimensions
list($width, $height) = getimagesize($filename);


function imagecreatefromjpegexif($filename, $w, $h)
{
    $newwidth = $w;
    $newheight = $h;
    $img = imagecreatefromjpeg($filename);
    $exif = exif_read_data($filename);
    if ($img && $exif && isset($exif['Orientation']))
    {
        $ort = $exif['Orientation'];

        if ($ort == 6 || $ort == 5)
            $img = imagerotate($img, 270, null);
        if ($ort == 3 || $ort == 4)
            $img = imagerotate($img, 180, null);
        if ($ort == 8 || $ort == 7) 
            $img = imagerotate($img, 90, null);

        if ($ort == 5 || $ort == 4 || $ort == 7)
            imageflip($img, IMG_FLIP_HORIZONTAL);
            
        if ($ort == 6 || $ort == 5 || $ort == 8 || $ort == 7) {
            $newwidth = $h;
            $newheight = $w;
        }
    }
    return array($img, $newwidth, $newheight);
}

$extension = strtolower(pathinfo($filename, PATHINFO_EXTENSION)); 
switch ($extension) {
    case 'jpg':
    case 'jpeg':
       list($source, $width, $height) = imagecreatefromjpegexif($filename, $width, $height);
    break;
    case 'gif':
       $source = imagecreatefromgif($filename);
    break;
    case 'png':
       $source = imagecreatefrompng($filename);
    break;
}

// Calcul des nouvelles dimensions
$min_width = 200;
if($min_width>$width || $min_width>$height) {
  $newheight = $height;
  $newwidth = $width;
}
else if($width>$height) {
  $newheight = $min_width;
  $newwidth = $width * $min_width / $height;
}
else {
  $newwidth = $min_width;
  $newheight = $height * $min_width / $width;
}

// Chargement
$thumb = imagecreatetruecolor($newwidth, $newheight);

// Redimensionnement
imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

//Chargement ...
if(!$previewalert){
  $texte = "Chargement ...";
  $blanc = imagecolorallocate($thumb, 255, 255, 255);
  $font = 5;
  $x = max(0, $newwidth/2-50);
  $y = $newheight/2;
  imagestring($thumb, $font, $x, $y, $texte, $blanc);
}
// Affichage
switch ($extension) {
    case 'jpg':
    case 'jpeg':
       imagejpeg($thumb);
    break;
    case 'gif':
	imagegif($thumb);
    break;
    case 'png':
       imagepng($thumb);
    break;
}

?>

