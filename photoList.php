<?php
header('Content-Type: application/json;charset=utf-8');

$data_dir = "data/";
$json_data = ['photos'=> []];

if(isset($_GET["album"])) {
    //Get a list of file paths using the glob function.
    $fileList = glob($data_dir.$_GET["album"]."/*");
 
    //Loop through the array that glob returned.
    foreach($fileList as $filename){
       array_push($json_data['photos'], $filename); 
    }
}  

echo json_encode($json_data);
?>
