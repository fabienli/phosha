<?php

header('Content-Type: application/json;charset=utf-8');

$json_data = ['status' => 'ok'];
$json_data['msg']='';

$tmp_file = $_FILES["file"]["tmp_name"];
$target_dir = "data/";
$uploadOk = 1;
// Check if image file is a actual image or fake image
if(isset($_FILES["file"]) && isset($_POST["album"])) {
  $target_dir .= $_POST["album"] ;
  mkdir($target_dir);
  $target_file = $target_dir ."/". basename($_FILES["file"]["name"]);
  $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

  $check = getimagesize($tmp_file);
  if($check !== false) {
    $json_data['msg'].= "File is an image - " . $check["mime"] . ".";
        // Check if file already exists
    if (file_exists($target_file)) {
      $json_data['msg'].= "Sorry, file already exists: ".$target_file;
      $uploadOk = 0;
    }
    else {
      $json_data['msg'].= "File ok";
      move_uploaded_file($tmp_file, $target_file);
      $uploadOk = 1;
    }
  } else {
    $json_data['msg'].= "File is not an image.";
    $uploadOk = 0;
  }
}
else {
  $json_data['msg'].= "Error: no file submitted:";
  //print_r($_FILES);   echo"-";   print_r($_POST);
}

$json_data['id'] = $_POST["id"];
if($uploadOk==0){
    $json_data['status'] = "ko";
}
echo json_encode($json_data);
?>
