# PhoSha

![PhoSha icon](img/favicon.png)

Decentralized collaborative photo albums:
  * PhoSha is a web app that anyone can deploy to share photo albums.
  * Anyone who has access to the album can then participate by adding more photo.
