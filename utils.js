//first, ensure https is used:
if (location.protocol !== 'https:') {
    location.replace(`https:${location.href.substring(location.protocol.length)}`);
}


const SITE_DOMAIN = "your.domain";
const ALBUMS_LIST = '/albumsList';
var albumList = new Set();
var nextId = 1;
var imageLoadStack = [];

function getDataUrl() {
    var data_url = {};
    var url = window.location.toString();
    var url_match = url.match(/\?([^#]*)/i);
    if(url_match !== null) {
        var url_data = url_match[1]; // gets the string between '?' and '#'
        var ar_url_data = url_data.split('&');
        for(var i=0; i<ar_url_data.length; i++) {
            var ar_val = ar_url_data[i].split('='); // separate name and value from each pair
            data_url[ar_val[0]] = ar_val[1];
        }
    }
    return data_url;
}

function start(){
    var data_url = getDataUrl();
    //console.log(data_url);
    // specific album:
    if (data_url['album'] != null) {
        var albumName = data_url['album'];
        if (!isValidAlbumName(albumName)) {
            location = '/';
            return null;
        }
        document.getElementById('albumRef').innerHTML = data_url['album'];
        document.getElementById('createPart').hidden = true;
        document.getElementById('createPart-title').hidden = true;
        const fileInput = document.getElementById('file-input');
        fileInput.addEventListener('change', (e) => syncNewPhoto(data_url['album'], e.target.files, e));
        addAlbumInList(data_url['album']);
        fillPhotoList(data_url['album']);
        fetch('/addToCache#/?album='+albumName);
    }
    // album list (or create a new one):
    else {
        document.getElementById('photoList').hidden=true;
        document.getElementById('photoList-title').hidden=true;
        document.getElementById('albumactions').hidden=true;
        getAlbumsList().then(() => {
            var displaylist = Array.from(albumList);
            for(i=0; i<displaylist.length; i++){
                var a = document.createElement('a');
                var div = document.createElement('div');
                var img = document.createElement('img');
                div.classList.add('album');
                div.appendChild(a);
                div.appendChild(img);
                document.getElementById('albumsList').appendChild(div);
                a.innerHTML = displaylist[i];
                a.href = '?album=' + displaylist[i];
                a.hidden = true;
                img.alt = displaylist[i];
                div.onclick = function(){this.firstChild.click()};
                fillFirstPhotoList(displaylist[i], img);
            }
        });
    }
    appendSWversion();
    document.getElementById('help').hidden=true;
}

function addAlbumInList(albumName) {
    updateAlbumInCacheList(albumName, true);
}
function removeAlbumInList(albumName) {
    updateAlbumInCacheList(albumName, false);
}

function updateAlbumInCacheList(albumName, add) {
    getAlbumsList()
    .then(() => {
        if(add)
            albumList.add(albumName)
        else
            albumList.delete(albumName)
    })
    .then(() => {
        fetch(ALBUMS_LIST, { method: "POST", body: JSON.stringify({ albums: Array.from(albumList) })})
            .then((response) => {
                //console.log('added album to cache: '+albumName+" /"+response+"/"+albumList);
             })
    });
}


function getAlbumsList() {
    return fetch(ALBUMS_LIST).then(response => response.json()).then(data => {
        //console.log('Got', data, 'from cache');
        if(data['albums']!=undefined){
            for (let elem of data['albums']) {
                if (isValidAlbumName(elem)) {
                    albumList.add(elem);
                }
            }
        }
    });
}

/* get Service Worker version */
function appendSWversion() {
    fetch("/version").then(response => response.json()).then(data => {
        //console.log('version: '+data);
        document.getElementById('appJsVersion').value+="."+data['v'];
    })
    .catch(function(err){
        console.log('Error no version', err, 'reloading');
        window.location.reload();
    });
}

function fillPhotoList(album) {
    return fetch('photosList/'+album)
          .then(response => response.json())
          .then(data => {
              for(i=0;i<data['photos'].length;i++){
                  var photoSrc = data['photos'][i];
                  addAPictureInDisplayList(photoSrc, true);
                  //called when loaded: fetch('/addToCache#'+photoSrc);
                  fetch('/addToCache#preview.php?photo='+photoSrc);
                  //console.log('ensure to add image in cache:'+photoSrc);
              }
              setTimeout(()=>{loadPreLoadedStack()}, 400);
           });
}

function fillFirstPhotoList(album, img) {
    return fetch('photosList/'+album).then(response => response.json()).then(data => {
        for(i=0;i<data['photos'].length;i++){
            var photoSrc = data['photos'][i];
            img.src = 'preview.php?photo=' + photoSrc+'&a=1';
            break;
        }
    });
}

// Load the full stsack of preloaded images in one shot
function loadPreLoadedStackBulk() {
  while(imageLoadStack.length > 0) {
    var imgData = imageLoadStack.shift();
    loadImage(imgData[0], imgData[1], imgData[2]);
  }
}

// Load next image in stack
function loadPreLoadedStack() {
  if(imageLoadStack.length > 0) {
    var imgData = imageLoadStack.shift();
    loadImage(imgData[0], imgData[1], imgData[2]);
  }
}

function loadImage(img, div, imgsrc) {
    var imga = new Image();
    imga.onload = function() {
        img.src = imga.src;
        img.parentElement.style = 'background-color: black';
        // ensure image is now stored in cache
        fetch('/addToCache#'+imga.src);
        // check if there are more images to load next in the stack:
        loadPreLoadedStack();
    };
    imga.src = imgsrc;
    //setTimeout(()=>{imga.src = imgsrc;},2000);
}

function addAPictureInDisplayList(src, preloadonly){
    var div = document.createElement('div');
    //div.style = 'background-image: url("https://dl.lisiecki.fr/preview.php?photo='+src+'");';
    div.classList.add('img-box');
    document.getElementById('photosList').appendChild(div);
    var img = document.createElement('img');
    div.appendChild(img);
    img.src = 'preview.php?photo='+src;
    img.id = 'img'+nextId;
    img.classList.add('cache');
    div.id='div-'+img.id;
    div.classList.add('cache');
    div.onclick = function(){clickImageDiv(this)};
    //img.onload = function() {setTimeout(()=>{this.parentElement.style = 'background-color: black'},300)};
    img.parentElement.style = 'background-color: grey';
    nextId+= 1;
    if(preloadonly) {
      imageLoadStack.push([img, div, src]);
    }
    else {
      loadImage(img, div, src)
    }
    return img.id;
}

function clickImageDiv(elt) {
    //console.log(elt);
    div = document.getElementById('fullscreen-preview');
    if(div.classList.contains('fullscreen')) {
        div.classList.remove('fullscreen');
        div.firstChild.src = '';
        div.hidden = true;
        while (div.firstChild) {
            div.removeChild(div.firstChild);
        }
        var img = document.createElement('img');
        div.appendChild(img);
    }
    else {
        div.hidden = false;
        div.classList.add('fullscreen');
        div.firstChild.src = elt.firstChild.src;
    }
}

function setImageAsSyncOK(imgId){
    console.log('setImageAsSyncOK('+imgId+')');
    document.getElementById(imgId).classList.add('syncok');
    document.getElementById(imgId).classList.remove('sync');
    document.getElementById('div-'+imgId).classList.add('syncok');
    document.getElementById('div-'+imgId).classList.remove('sync');
}

function syncNewPhoto(album, fileList, e) {
    let file = null;

    for (let i = 0; i < fileList.length; i++) {
      if (fileList[i].type.match(/^image\//)) {
        file = fileList[i];

        if (file !== null) {
          var imgId = addAPictureInDisplayList(URL.createObjectURL(file), false);
          document.getElementById(imgId).classList.add('sync');
          document.getElementById('div-'+imgId).classList.add('sync');
          var data = new FormData();
          data.append('file', file);
          data.append('album', album);
          data.append('id', imgId);
          fetch('upload.php', { method: "POST", body: data}).then(response => response.json()).then(data => {
              //console.log(data);
              setImageAsSyncOK(data['id']);
              }
          ).catch(
            error => console.log(error) // Handle the error response object
          );
        }
      }
    }
}

function generateNewAlbumName() {
    var length           = 12;
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    if(isExistingAlbumName(result)) {
        // retry again
        // todo: protect from infinite loop (e.g.: increase length)
        return generateNewAlbumName();
    }
    return result;
}

function goToNewAlbum() {
    var newAlbumName = generateNewAlbumName();
    location = '/?album='+newAlbumName;
}

function isValidAlbumName(name) {
    return name.length > 10;
}

function isExistingAlbumName(name) {
    return false;
}


function forgetThisAlbum() {
    var data_url = getDataUrl();
    //console.log(data_url);
    // specific album:
    if (data_url['album'] != null) {
        var albumName = data_url['album'];
        removeAlbumInList(albumName)
    }
    alert('album oublié, il ne figure plus dans votre liste.');
    location = '/';
}

function shareThisAlbum() {
    var data_url = getDataUrl();
    if (data_url['album'] != null) {
        var albumName = data_url['album'];
        div = document.getElementById('fullscreen-preview');
        div.hidden = false;
        div.classList.add('fullscreen');
        var a = document.createElement('a');
        a.innerHTML = "Partager par email";
        a.href = "mailto:?subject=Album photo partag&eacute;&body=https://"+SITE_DOMAIN+"/?album="+albumName;
        var input = document.createElement('input');
        input.value = "https://"+SITE_DOMAIN+"/?album="+albumName;
        input.id='inputCopy';
        input.onclick = function(){copy()};
        div.appendChild(a);
        var a2 = document.createElement('a');
        a2.innerHTML = ", ou copier en cliquant dans : ";
        div.appendChild(a2);
        div.appendChild(input);
    }

}

function copy() {
  var copyText = document.querySelector("#inputCopy");
  copyText.select();
  document.execCommand("copy");
  alert('copié !');
}

function downloadAllThisAlbum() {
    var data_url = getDataUrl();
    if (data_url['album'] != null) {
        window.open('/dlAlbum.php?album='+data_url['album']);
    }
}

function deleteThisAlbum() {
    alert('non !');
}

/*
*************************************************
*/

//start();
