<?php 

if(isset($_GET['album'])){
  $albumName = $_GET['album'];
  if (checkName($albumName)) {
    if(isfileAlreadyCreated()) {
      //TODO: get $filename filled
    }
    else {
      // Create ZIP file
      $zip = new ZipArchive();
      $date = date_create();
      $timestamp = date_timestamp_get($date);
      $filename = "./data/album-".$albumName."-".$timestamp.".zip";
      if ($zip->open($filename, ZipArchive::CREATE)!==TRUE) {
        exit("cannot open <$filename>\n");
      }
      $dir = 'data/'.$albumName.'/';

      // Create zip
      createZip($zip, $dir);
      $zip->close();
    }

    downloadFile($filename);
  }
}
function checkName($album) {
  if (strlen($album)<10) 
    return false;
  if (strpos($album, '.') !== false) 
    return false;
  if (strpos($album, '/') !== false) 
    return false;

  return true;
}

function isfileAlreadyCreated() {
  //TODO: check if we can re-use a ZIP file already there
  return false;
}
  
// Create zip
function createZip($zip,$dir){
  if (is_dir($dir)){

    if ($dh = opendir($dir)){
       while (($file = readdir($dh)) !== false){
 
         // If file
         if (is_file($dir.$file)) {
            if($file != '' && $file != '.' && $file != '..'){
 
               $zip->addFile($dir.$file, $file);
            }
         }
         //do not recurse, as images are in flat folder:
         /*else{
            // If directory
            if(is_dir($dir.$file) ){

              if($file != '' && $file != '.' && $file != '..'){

                // Add empty directory
                $zip->addEmptyDir($dir.$file);

                $folder = $dir.$file.'/';
 
                // Read data of the folder
                createZip($zip,$folder);
              }
            }
 
         }*/
 
       }
       closedir($dh);
     }
  }
}

// Download Created Zip file
function downloadFile($filename) {

  if (file_exists($filename)) {
     header('Content-Type: application/zip');
     header('Content-Disposition: attachment; filename="'.basename($filename).'"');
     header('Content-Length: ' . filesize($filename));

     flush();
     readfile($filename);
     // delete file
     unlink($filename);
 
   }
}
?>
